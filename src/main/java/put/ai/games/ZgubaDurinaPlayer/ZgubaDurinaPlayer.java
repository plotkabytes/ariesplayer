package put.ai.games.ZgubaDurinaPlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import put.ai.games.aries.AriesMove;
import put.ai.games.game.Board;
import put.ai.games.game.Move;
import put.ai.games.game.Player;
import put.ai.games.game.Player.Color;

public class ZgubaDurinaPlayer extends Player {

	public static void main(String[] args) {

	}

	@Override
	public String getName() {
		return "Mateusz Żyła";
	}

	@Override
	public Move nextMove(Board board) {

		Color me = getColor();

		Board clonedBoard = board.clone();

		Move m = null;
		NegaScout ns = new NegaScout(clonedBoard, me);
		ExecutorService exService = Executors.newSingleThreadExecutor();
		FutureTask<Move> task = new FutureTask<Move>(ns);

		exService.execute(task);

		try {

			m = task.get(getTime() - 5, TimeUnit.MILLISECONDS);
			return m;

		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			return ns.getCurrentBestMove();
		}
	}
}

class NegaScout implements Callable<Move> {

	private int boardSize;
	private int ramValue = 1;
	private int maxDepth = 2;
	private int goalX;
	private int goalY;
	private int enemyGoalX;
	private int enemyGoalY;
	private int infinity = 1000;

	private boolean isStopped = false;
	private Random random = new Random(0xdeadbeef);

	private Move currentBestMove;
	private Board board;
	private Color me;
	private ArrayList<AriesMove> madeMoves;
	private ArrayList<Color> madeMovesColors;

	public NegaScout(Board b, Color me) {

		this.board = b;
		this.me = me;
		this.boardSize = b.getSize();
		this.madeMoves = new ArrayList<AriesMove>();
		this.madeMovesColors = new ArrayList<Color>();

		if (this.me.compareTo(Color.PLAYER2) == 0) {

			this.goalX = 0;
			this.goalY = 0;

			this.enemyGoalX = boardSize;
			this.enemyGoalY = boardSize;

		} else {

			this.enemyGoalX = 0;
			this.enemyGoalY = 0;

			this.goalX = boardSize;
			this.goalY = boardSize;
		}

	}

	/*
	 * Moze powinienem liczyc wartosc boarda zamiast wartosci ruchow ?
	 * Wtedy glebokosc drzewa musi byc nieparzysta.
	 */
	public int NegaScoutSearch(Color player, int alpha, int beta, int depth) {

		if (isStopped()) {
			return 0;
		}

		if (depth == 0) {

			int val = evaluate();
			return val;
		}

		int best = -infinity;
		int n = beta;

		List<Move> availableMoves = board.getMovesFor(player);

		if (!availableMoves.isEmpty()) {

			if (availableMoves.size() > 0 && depth == maxDepth) {
				setCurrentBestMove((AriesMove) availableMoves.get(0));
			}

			while (availableMoves.size() > 0 && best < beta) {

				AriesMove m = (AriesMove) availableMoves.get(0);
				availableMoves.remove(0);

				madeMoves.add(m);
				madeMovesColors.add(player);
				board.doMove(m);

				int est = -NegaScoutSearch(Player.getOpponent(player), -n, -Math.max(alpha, best), depth - 1);
				
				if (est > best) {
					if (n == beta || depth <= 2) {
						best = est;
						if (depth == maxDepth)
							setCurrentBestMove(m);
					} else {
							best = -NegaScoutSearch(Player.getOpponent(player), -beta, -est, depth - 1);

						if (depth == maxDepth)
							setCurrentBestMove(m);
					}
				}

				madeMoves.remove(m);
				madeMovesColors.remove(player);
				board.undoMove(m);

				n = Math.max(alpha, best) + 1;

			}
		}
		return best;
	}

	public int evaluate() {

		int val = 0;

		Board copyOfBoard = this.board.clone();

		ArrayList<AriesMove> reversedMoves = new ArrayList<AriesMove>(madeMoves);
		ArrayList<Color> reversedMovesColors = new ArrayList<Color>(madeMovesColors);

		Collections.reverse(reversedMoves);
		Collections.reverse(reversedMovesColors);

		AriesMove winningMove = reversedMoves.get(1);
		
		// jesli moj pierwszy ruch jest wygrywajacy to go zrob
		if(winningMove.getDstX() == goalX && winningMove.getDstY() == goalY)
			return 10000;
		
		for (int i = 0; i < reversedMoves.size(); i++) {

			Color player = reversedMovesColors.get(i);

			if (player.compareTo(this.me) == 0) {
				copyOfBoard.undoMove(reversedMoves.get(i));
				val += evaluateMoveValue(reversedMoves.get(i), copyOfBoard);
			} else {
				copyOfBoard.undoMove(reversedMoves.get(i));
			}

		}

		return val;
	}

	// try to defend
	// not only attack
	// try to find move that enemy cannot block
	public int evaluateMoveValue(AriesMove move, Board clonedBoard) {

		int value = 0;
		int valBefore = 0;
		int valAfter = 0;
		
		// jesli jest to ruch atakujacy...
		if (clonedBoard.getState(move.getDstX(), move.getDstY()) != me
				&& clonedBoard.getState(move.getDstX(), move.getDstY()) != Color.EMPTY) {
			
			value += 20;

			// i jesli to ruch rozjebujacy piona
			if (this.me.compareTo(Color.PLAYER1) == 0) {
				valBefore = evaluateGameValue(Color.PLAYER2,clonedBoard);
				Board tempBoard = clonedBoard.clone();
				tempBoard.doMove(move);
				valAfter = evaluateGameValue(Color.PLAYER2,tempBoard);
				
				if(valAfter < valBefore)
					value += 20;
			}
			
		}

		// jesli jest to ruch wygrywajacy...
		if (move.getDstX() == goalX && move.getDstY() == goalY) {
			value += 20;
		}

		// jesli wrog jest blisko...
		for (int i = 0; i < this.boardSize; i++) {
			for (int j = 0; j < this.boardSize; j++) {
				if(clonedBoard.getState(i, j) != me && clonedBoard.getState(i, j) != Color.EMPTY) {
					
					int enemyValueX = Math.abs(i - enemyGoalX);
					int enemyValueY = Math.abs(j - enemyGoalY);
					
					if(enemyValueX + enemyValueY < 6 && move.getDstX() == i && move.getDstY() == j)
						value += 20;	
				}
			}
		}

		int valueX = Math.abs(move.getDstX() - goalX);
		int valueY = Math.abs(move.getDstY() - goalY);

		// trzeba zawsze byc jak najblizej punktu zwyciestwa...
		value -= (valueX + valueY);
		return value;
	}
	
	// if(j == enemyGoalY && Math.abs(i - enemyGoalX) > 4 || i == enemyGoalX && Math.abs(j - enemyGoalX) > 4 );
	// do not attack it!
	
	/**
	 * Liczy wartosc calej gry dla danego gracza.
	 */
	public int evaluateGameValue(Color playerColor,Board clonedBoard) {

		int value = 0;

		for (int i = 0; i < this.boardSize; i++)
			for (int j = 0; j < this.boardSize; j++) {
				if (clonedBoard.getState(i, j) == playerColor) {
					value += ramValue;
				}
			}

		return value;
	}

	public Move run() {
		NegaScoutSearch(me, -infinity, infinity, maxDepth);
		return getCurrentBestMove();
	}

	public boolean isStopped() {
		return isStopped;
	}

	public void setStopped(boolean isStopped) {
		this.isStopped = isStopped;
	}

	@Override
	public Move call() throws Exception {
		NegaScoutSearch(me, -infinity, infinity, maxDepth);
		return getCurrentBestMove();
	}

	public Move getCurrentBestMove() {
		if (currentBestMove != null)
			return currentBestMove;
		else {
			List<Move> moves = this.board.getMovesFor(me);
			return moves.get(random.nextInt(moves.size()));
		}
	}

	public void setCurrentBestMove(Move currentBestMove) {
		this.currentBestMove = currentBestMove;
	}

}